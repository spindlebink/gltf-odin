package gltf

import "core:image"
import "core:math/linalg"

@(private)
SBYTE :: 5120
@(private)
UBYTE :: 5121
@(private)
SSHORT :: 5122
@(private)
USHORT :: 5123
@(private)
UINT :: 5125
@(private)
FLOAT :: 5126

SByte :: i8
UByte :: u8
SShort :: i16
UShort :: u16
UInt :: u32
Float :: f32
Quat :: linalg.Quaternionf32

GLTF :: struct {
	root_scene: ^Scene,
	scenes: []Scene,
	nodes: []Node,
	meshes: []Mesh,
	materials: []Material,
	animations: []Animation,
	skins: []Skin,
	buffers: [][]byte,
	images: []^image.Image,
	textures: []Texture,
	samplers: []Texture_Sampler,
}

Scene :: struct {
	name: string,
	nodes: []^Node,
}

Node :: struct {
	name: string,
	translation: [3]Float,
	rotation: Quat,
	scale: [3]Float,
	mesh: ^Mesh,
	children: []^Node,
}

Mesh :: struct {
	name: string,
	primitives: []Primitive,
}

Material :: struct {
	name: string,
	pbr_metallic_roughness: struct {
		base_color_factor: [4]Float,
		metallic_factor: Float,
		roughness_factor: Float,
		// TODO: PBR textures
	},
}

Texture :: struct {
	name: string,
	source: ^image.Image,
	sampler: ^Texture_Sampler,
}

Texture_Sampler :: struct {
	name: string,
	mag_filter: Texture_Filtering,
	min_filter: Texture_Filtering,
	wrap_s: Texture_Wrapping,
	wrap_t: Texture_Wrapping,
}

Texture_Filtering :: enum uint {
	Nearest = 9728,
	Linear = 9729,
	Nearest_Mipmap_Nearest = 9984,
	Linear_Mipmap_Nearest = 9985,
	Nearest_Mipmap_Linear = 9986,
	Linear_Mipmap_Linear = 9987,
}

Texture_Wrapping :: enum uint {
	Repeat = 10497,
	Mirrored_Repeat = 33648,
	Clamp_To_Edge = 33071,
}

Animation :: struct {
	name: string,
	channels: []Animation_Channel,
	samplers: []Animation_Sampler,
}

Animation_Channel :: struct {
	sampler: ^Animation_Sampler,
	target: struct {
		node: ^Node,
		path: Animation_Path,
	},
}

Animation_Sampler :: struct {
	input: []Float,
	output: Accessor,
	interpolation: Interpolation_Mode,
}

Animation_Path :: enum {
	Translation,
	Rotation,
	Scale,
	Weights,
}

Interpolation_Mode :: enum {
	Linear,
	Step,
	Cubic_Spline,
}

Skin :: struct {
	skeleton: ^Node,
	joints: []^Node,
	inverse_bind_matrices: []matrix[4, 4]Float,
}

Primitive :: struct {
	material: ^Material,
	material_index: uint,
	indices: Accessor,
	position: [][3]Float,
	normal: [][3]Float,
	tangent: [][4]Float,
	tex_coord: []Accessor,
	color: []Accessor,
	joints: []Accessor,
	weights: []Accessor,
}

Accessor :: union {
	// SCALAR
	[]SByte,
	[]UByte,
	[]SShort,
	[]UShort,
	[]UInt,
	[]Float,
	// VEC2
	[][2]SByte,
	[][2]UByte,
	[][2]SShort,
	[][2]UShort,
	[][2]UInt,
	[][2]Float,
	// VEC3
	[][3]SByte,
	[][3]UByte,
	[][3]SShort,
	[][3]UShort,
	[][3]UInt,
	[][3]Float,
	// VEC4
	[][4]SByte,
	[][4]UByte,
	[][4]SShort,
	[][4]UShort,
	[][4]UInt,
	[][4]Float,
	// MAT2
	[]matrix[2, 2]SByte,
	[]matrix[2, 2]UByte,
	[]matrix[2, 2]SShort,
	[]matrix[2, 2]UShort,
	[]matrix[2, 2]UInt,
	[]matrix[2, 2]Float,
	// MAT3
	[]matrix[3, 3]SByte,
	[]matrix[3, 3]UByte,
	[]matrix[3, 3]SShort,
	[]matrix[3, 3]UShort,
	[]matrix[3, 3]UInt,
	[]matrix[3, 3]Float,
	// MAT4
	[]matrix[4, 4]SByte,
	[]matrix[4, 4]UByte,
	[]matrix[4, 4]SShort,
	[]matrix[4, 4]UShort,
	[]matrix[4, 4]UInt,
	[]matrix[4, 4]Float,
}

@(private)
accessor_stride :: proc(type: string, component: uint) -> uint {
	mult := uint(0)
	size := uint(0)
	switch type {
	case "SCALAR": mult = 1
	case "VEC2": mult = 2
	case "VEC3": mult = 3
	case "VEC4": mult = 4
	case "MAT2": mult = 2 * 2
	case "MAT3": mult = 3 * 3
	case "MAT4": mult = 4 * 4
	}
	switch component {
	case SBYTE: size = size_of(SByte)
	case UBYTE: size = size_of(UByte)
	case SSHORT: size = size_of(SShort)
	case USHORT: size = size_of(UShort)
	case UINT: size = size_of(UInt)
	case FLOAT: size = size_of(Float)
	}
	return mult * size
}
