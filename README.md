# glTF Loader

A WIP glTF loader in [the Odin programming language](https://odin-lang.org).

## License

BSD 3-clause.
